﻿using System;
using System.Collections.Generic;
using System.Linq;
using DesafioVulcao.Interfaces;
using DesafioVulcao.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace DesafioVulcao.Controllers
{

   
    public class GraficoController : Controller
    {
        private readonly IRegrasServicos _regrasServicos;

        public GraficoController(IRegrasServicos regrasServicos)
        {
            _regrasServicos = regrasServicos;
        }
        public IActionResult Grafico()
        {            
            InfoInicial info = new InfoInicial();
            ViewData["ListaRelatorioFumaca"] = new List<RelatorioFumaca>();
            return View(info);
        }
        [HttpPost]
        public IActionResult IniciarCalculo(InfoInicial infoInicial)
        {
            ViewData["ListaRelatorioFumaca"] = new List<RelatorioFumaca>();
            if (ModelState.IsValid)
            {
                List<RelatorioFumaca> listaRelatorioPorDias = _regrasServicos.DefinirLocalFumaca(infoInicial.QtdAeroporto, infoInicial.QtdFumaca, infoInicial.TamanhoX, infoInicial.TamanhoY);
                listaRelatorioPorDias = _regrasServicos.calcularFumacaPorDia(listaRelatorioPorDias[0].listaInfoFumaca);

                listaRelatorioPorDias = listaRelatorioPorDias.OrderBy(T => T.dia).ToList();
                ViewData["ListaRelatorioFumaca"] = listaRelatorioPorDias;
                return View("Grafico", infoInicial);
            }
            return View("Grafico", infoInicial);
        }
        
        //API retorno
        [HttpGet]
        public IActionResult ObterResultado([FromBody] InfoInicial infoInicial)
        {
            if(ModelState.IsValid)
            { 
            List<RelatorioFumaca> relatorioDiaZero = _regrasServicos.DefinirLocalFumaca(infoInicial.QtdAeroporto, infoInicial.QtdFumaca, infoInicial.TamanhoX, infoInicial.TamanhoY);
            List<RelatorioFumaca> listaRelatorioPorDias = _regrasServicos.calcularFumacaPorDia(relatorioDiaZero[0].listaInfoFumaca);
            return Ok(listaRelatorioPorDias);
            }
            return UnprocessableEntity(ModelState);
        }

    }
}
