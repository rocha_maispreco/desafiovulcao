﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioVulcao.Models
{
    public class InfoFumaca
    {
        public int posicaoX { get; set; }
        public int posicaoY { get; set; }
        public bool Nova { get; set; }
        public bool Aeroporto { get; set; }    
        public bool TemFumaca { get; set; }
        public int? diaFumaca { get; set; }
        public bool PossuiaAeroporto { get; set; }
    }

}
