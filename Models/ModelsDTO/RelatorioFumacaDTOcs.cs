﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioVulcao.Models.ModelsDTO
{
    public class RelatorioFumacaDTOcs
    {
        public int QtdFumaca { get; set; }
     
        public int QtdAeroporto { get; set; }
  
        public int TamanhoX { get; set; }
       
        public int TamanhoY { get; set; }
    }
}
