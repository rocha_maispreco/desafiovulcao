﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioVulcao.Models
{
    public class InfoInicial
    {
        [Required]
        [Range(4,20, ErrorMessage = "Quantidade miníma de 4 e máximo de 20.")]
        public int QtdFumaca { get; set; }

        [Required]
        [Range(3, 20, ErrorMessage = "Quantidade miníma de 3 e máximo de 20.")]
        
        public int QtdAeroporto { get; set; }

        [Required]
        [Range(10, 100, ErrorMessage = "Tamanho minímo de 10 e máximo de 100.")]
        public int TamanhoX { get; set; }
        [Required]
        [Range(10, 100, ErrorMessage = "Tamanho minímo de 10 e máximo de 100.")]
        public int TamanhoY { get; set; }
    }
}
