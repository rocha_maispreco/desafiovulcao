﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioVulcao.Models
{
    public class RelatorioFumaca
    {
        public int dia { get; set; }
        public List<InfoFumaca> listaInfoFumaca { get; set; }
        public bool TemAeroportoLimpo { get; set; }
        public int? primeiroAeroporto { get; set; }
    }
}
