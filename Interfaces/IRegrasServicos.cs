﻿using DesafioVulcao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioVulcao.Interfaces
{
    public interface IRegrasServicos
    {
        List<RelatorioFumaca> calcularFumacaPorDia(List<InfoFumaca> listaInfoFumacaDiaZero);
        List<RelatorioFumaca> DefinirLocalFumaca(int qtdAeroporto, int qtdFumaca, int tamanhoX, int tamanhoY);
    }
}
