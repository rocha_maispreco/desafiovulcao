﻿using DesafioVulcao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioVulcao.Interfaces.Servicos
{
    public class RegrasServicos : IRegrasServicos
    {

        public  List<RelatorioFumaca> calcularFumacaPorDia(List<InfoFumaca> listaInfoFumacaDiaZero)

        {


            List<RelatorioFumaca> listaRelatorioPorDias = new List<RelatorioFumaca>();
            //Setando dia 0
            listaRelatorioPorDias.Add(new RelatorioFumaca { dia = 0, listaInfoFumaca = listaInfoFumacaDiaZero, TemAeroportoLimpo = true });

            int? primeiroAeroportoAux = null;
            List<InfoFumaca> listaInfoFumacaAux = new List<InfoFumaca>();
            listaInfoFumacaAux.AddRange(listaInfoFumacaDiaZero);

            bool temAeroportoLimpo = true;
            int diaAtualCalculado = 0;
            while (temAeroportoLimpo)
            {

                diaAtualCalculado++;

                //Resetando váriavel "Nova" que indica se a fumaça foi criada no dia do calculo ou se é uma fumaça antiga.
                listaInfoFumacaAux.Where(T => T.Nova == true).ToList().ForEach(T => T.Nova = false);
                List<InfoFumaca> listaNovoDia = new List<InfoFumaca>();

               
                foreach (InfoFumaca infoFumaca in listaInfoFumacaAux)
                {
                    if (infoFumaca.TemFumaca && infoFumaca.diaFumaca == diaAtualCalculado - 1)
                    {

                        //Adicionar uma posição a frente de X
                        InfoFumaca infoFumacaAux = listaInfoFumacaAux.Where(T => T.posicaoX == infoFumaca.posicaoX + 1
                                                                           && T.posicaoY == infoFumaca.posicaoY
                                                                           && T.diaFumaca == null).FirstOrDefault();
                        if (infoFumacaAux != null)
                        {
                            if (listaNovoDia.Where(t => t.posicaoX == infoFumacaAux.posicaoX && t.posicaoY == infoFumacaAux.posicaoY).FirstOrDefault() == null)
                            {
                                InfoFumaca infoFumacaAdicionar = new InfoFumaca();


                                infoFumacaAdicionar.Nova = true;
                                infoFumacaAdicionar.TemFumaca = true;
                                infoFumacaAdicionar.diaFumaca = diaAtualCalculado;
                                infoFumacaAdicionar.Aeroporto = infoFumacaAux.Aeroporto;
                                infoFumacaAdicionar.posicaoX = infoFumacaAux.posicaoX;
                                infoFumacaAdicionar.posicaoY = infoFumacaAux.posicaoY;
                                if (infoFumacaAux.Aeroporto == true)
                                {
                                    if (primeiroAeroportoAux == null)
                                        primeiroAeroportoAux = diaAtualCalculado;
                                    infoFumacaAdicionar.PossuiaAeroporto = true;
                                    infoFumacaAdicionar.Aeroporto = false;
                                }

                                listaNovoDia.Add(infoFumacaAdicionar);
                            }
                        }
                        //Adicionar uma posição atrás de X
                        infoFumacaAux = listaInfoFumacaAux.Where(T => T.posicaoX == infoFumaca.posicaoX - 1
                                                                           && T.posicaoY == infoFumaca.posicaoY
                                                                           && T.diaFumaca == null).FirstOrDefault();
                        if (infoFumacaAux != null)
                        {
                            if (listaNovoDia.Where(t => t.posicaoX == infoFumacaAux.posicaoX && t.posicaoY == infoFumacaAux.posicaoY).FirstOrDefault() == null)
                            {
                                InfoFumaca infoFumacaAdicionar = new InfoFumaca();
                                listaNovoDia.Remove(infoFumacaAux);

                                infoFumacaAdicionar.Nova = true;
                                infoFumacaAdicionar.TemFumaca = true;
                                infoFumacaAdicionar.diaFumaca = diaAtualCalculado;
                                infoFumacaAdicionar.Aeroporto = infoFumacaAux.Aeroporto;
                                infoFumacaAdicionar.posicaoX = infoFumacaAux.posicaoX;
                                infoFumacaAdicionar.posicaoY = infoFumacaAux.posicaoY;
                                if (infoFumacaAux.Aeroporto == true)
                                {
                                    if (primeiroAeroportoAux == null)
                                        primeiroAeroportoAux = diaAtualCalculado;
                                    infoFumacaAdicionar.PossuiaAeroporto = true;
                                    infoFumacaAdicionar.Aeroporto = false;
                                }



                                listaNovoDia.Add(infoFumacaAdicionar);
                            }
                        }


                        //Adicionar uma posição acima Y
                        infoFumacaAux = listaInfoFumacaAux.Where(T => T.posicaoX == infoFumaca.posicaoX
                                                                           && T.posicaoY == infoFumaca.posicaoY + 1
                                                                           && T.diaFumaca == null).FirstOrDefault();
                        if (infoFumacaAux != null)
                        {
                            if (listaNovoDia.Where(t => t.posicaoX == infoFumacaAux.posicaoX && t.posicaoY == infoFumacaAux.posicaoY).FirstOrDefault() == null)
                            {
                                InfoFumaca infoFumacaAdicionar = new InfoFumaca();
                                listaNovoDia.Remove(infoFumacaAux);

                                infoFumacaAdicionar.Nova = true;
                                infoFumacaAdicionar.TemFumaca = true;
                                infoFumacaAdicionar.diaFumaca = diaAtualCalculado;
                                infoFumacaAdicionar.Aeroporto = infoFumacaAux.Aeroporto;
                                infoFumacaAdicionar.posicaoX = infoFumacaAux.posicaoX;
                                infoFumacaAdicionar.posicaoY = infoFumacaAux.posicaoY;
                                if (infoFumacaAux.Aeroporto == true)
                                {
                                    if (primeiroAeroportoAux == null)
                                        primeiroAeroportoAux = diaAtualCalculado;
                                    infoFumacaAdicionar.PossuiaAeroporto = true;
                                    infoFumacaAdicionar.Aeroporto = false;
                                }



                                listaNovoDia.Add(infoFumacaAdicionar);
                            }
                        }


                        //Adicionar uma posição abaixo Y
                        infoFumacaAux = listaInfoFumacaAux.Where(T => T.posicaoX == infoFumaca.posicaoX
                                                                           && T.posicaoY == infoFumaca.posicaoY - 1
                                                                           && T.diaFumaca == null).FirstOrDefault();
                        if (infoFumacaAux != null)
                        {
                            if (listaNovoDia.Where(t => t.posicaoX == infoFumacaAux.posicaoX && t.posicaoY == infoFumacaAux.posicaoY).FirstOrDefault() == null)
                            {
                                InfoFumaca infoFumacaAdicionar = new InfoFumaca();
                                listaNovoDia.Remove(infoFumacaAux);

                                infoFumacaAdicionar.Nova = true;
                                infoFumacaAdicionar.TemFumaca = true;
                                infoFumacaAdicionar.diaFumaca = diaAtualCalculado;
                                infoFumacaAdicionar.Aeroporto = infoFumacaAux.Aeroporto;
                                infoFumacaAdicionar.posicaoX = infoFumacaAux.posicaoX;
                                infoFumacaAdicionar.posicaoY = infoFumacaAux.posicaoY;
                                if (infoFumacaAux.Aeroporto == true)
                                {
                                    if (primeiroAeroportoAux == null)
                                        primeiroAeroportoAux = diaAtualCalculado;
                                    infoFumacaAdicionar.PossuiaAeroporto = true;
                                    infoFumacaAdicionar.Aeroporto = false;
                                }

                                listaNovoDia.Add(infoFumacaAdicionar);
                            }
                        }



                    }

                }

                //Adicionando espaços sem fumaça.
                var listaNaoAdicionada = listaInfoFumacaAux.Where(T => T.TemFumaca == false || T.Aeroporto == true || T.diaFumaca < diaAtualCalculado).ToList();
                foreach (InfoFumaca item in listaNaoAdicionada)
                {
                    InfoFumaca infoFumacaAdicionar = new InfoFumaca();
                    if (listaNovoDia.Where(T => T.posicaoX == item.posicaoX && T.posicaoY == item.posicaoY).FirstOrDefault() == null)
                    {
                        infoFumacaAdicionar = item;
                        listaNovoDia.Add(infoFumacaAdicionar);
                    }

                }



                listaInfoFumacaAux = new List<InfoFumaca>();              
                listaInfoFumacaAux.AddRange(listaNovoDia);

                //Verificando se ainda possui aeroporto sem fumaça.
                if (listaNovoDia.Where(T => T.Aeroporto == true).FirstOrDefault() == null)
                    temAeroportoLimpo = false;
                //Ordenando lista do dia.
                listaNovoDia = listaNovoDia.OrderBy(T => T.posicaoY).OrderBy(T => T.posicaoX).ToList();
                //Verificando se esse foi o dia em que primeiro aeroporto foi coberto por fumaça.
                if (primeiroAeroportoAux > 0)
                    listaRelatorioPorDias.Add(new RelatorioFumaca { dia = diaAtualCalculado, listaInfoFumaca = listaNovoDia, TemAeroportoLimpo = temAeroportoLimpo, primeiroAeroporto = primeiroAeroportoAux });
                else
                    listaRelatorioPorDias.Add(new RelatorioFumaca { dia = diaAtualCalculado, listaInfoFumaca = listaNovoDia, TemAeroportoLimpo = temAeroportoLimpo });

            }



            return listaRelatorioPorDias;
        }

        //Definir os pontos iniciais de modo aleátorio.
        public List<RelatorioFumaca> DefinirLocalFumaca(int qtdAeroporto, int qtdFumaca, int tamanhoX, int tamanhoY)
        {
            List<RelatorioFumaca> listaRelatorioPorDias = new List<RelatorioFumaca>();


            //Adicionando Fumaça
            RelatorioFumaca relatorio = new RelatorioFumaca();
            List<Tuple<int, int>> listaAleatoriFumaca = new List<Tuple<int, int>>();
            for (int i = 0; i < qtdFumaca; i++)
            {
                bool localValido = false;
                Random rr = new Random();
                while (!localValido)
                {
                    int valorX = rr.Next(1, tamanhoX);
                    int valorY = rr.Next(1, tamanhoY);
                    Tuple<int, int> ts = new Tuple<int, int>(valorX, valorY);

                    if (!listaAleatoriFumaca.Contains(ts))
                    {
                        listaAleatoriFumaca.Add(ts);
                        localValido = true;
                    }
                }
            }


            //Adicionando Aeroportos  
            List<Tuple<int, int>> listaAleatoriAeroporto = new List<Tuple<int, int>>();
            for (int i = 0; i < qtdAeroporto; i++)
            {
                bool localValido = false;
                Random rr = new Random();
                while (!localValido)
                {
                    int valorX = rr.Next(1, tamanhoX);
                    int valorY = rr.Next(1, tamanhoY);
                    Tuple<int, int> ts = new Tuple<int, int>(valorX, valorY);

                    if (!listaAleatoriFumaca.Contains(ts) && !listaAleatoriAeroporto.Contains(ts))
                    {
                        listaAleatoriAeroporto.Add(ts);
                        localValido = true;
                    }
                }
            }
            //Criando lista final
            List<InfoFumaca> ListaInfoPosicao = new List<InfoFumaca>();

            for (int x = 1; x <= tamanhoX; x++)
            {
                for (int y = 1; y <= tamanhoY; y++)
                {
                    InfoFumaca infoFumaca = new InfoFumaca();
                    Tuple<int, int> ts = new Tuple<int, int>(x, y);

                    if (listaAleatoriAeroporto.Contains(ts))
                        infoFumaca.Aeroporto = true;
                    if (listaAleatoriFumaca.Contains(ts))
                    {
                        infoFumaca.TemFumaca = true;
                        infoFumaca.diaFumaca = 0;
                    }
                    infoFumaca.posicaoX = x;
                    infoFumaca.posicaoY = y;
                    infoFumaca.Nova = false;


                    ListaInfoPosicao.Add(infoFumaca);
                }
            }
            relatorio.dia = 0;
            relatorio.TemAeroportoLimpo = true;
            relatorio.listaInfoFumaca = new List<InfoFumaca>(ListaInfoPosicao);
            listaRelatorioPorDias.Add(relatorio);
            return listaRelatorioPorDias;
            
        }
    }
}
